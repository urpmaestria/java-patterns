package com.emedinaa.urp.dto;

import java.util.ArrayList;
import java.util.List;

public class StudentBo {

    private List<StudentVo> students = new ArrayList<>();

    public StudentBo() {
        StudentVo student1 = new StudentVo("Robert", 0);
        StudentVo student2 = new StudentVo("John", 1);
        students.add(student1);
        students.add(student2);
    }

    public List<StudentVo> getAll() {
        return students;
    }

    public void add(StudentVo studentVo) {
        if (students.contains(studentVo)) {
            return;
        }
        students.add(studentVo);
        System.out.println("StudentVo add  -  role: " + studentVo.getRoll() + " name : " + studentVo.getName());
    }
}
