package com.emedinaa.urp.dto;

import java.util.List;

/**
 * Data transfer objet Pattern (DTO/VO)
 */
public class Main {

    public static void main(String[] args) {
        StudentBo studentBo = new StudentBo();
        printData(studentBo.getAll());

        StudentVo studentVo = new StudentVo("Pepito", 2);
        studentBo.add(studentVo);
        printData(studentBo.getAll());
    }
    //out
    /*
        StudentVo : Roll 0 name  Robert
        StudentVo : Roll 1 name  John

        StudentVo add  -  role: 2 name : Pepito
        StudentVo : Roll 0 name  Robert
        StudentVo : Roll 1 name  John
        StudentVo : Roll 2 name  Pepito
     */

    static void printData(List<StudentVo> students) {
        students.forEach(element -> {
            System.out.println("StudentVo : Roll " + element.getRoll() + " name  " + element.getName());
        });
    }
}
