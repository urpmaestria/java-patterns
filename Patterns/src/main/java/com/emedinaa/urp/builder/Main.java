package com.emedinaa.urp.builder;

/**
 * Builder Pattern
 * https://www.digitalocean.com/community/tutorials/builder-design-pattern-in-java
 */
public class Main {


    public static void main(String[] args) {
        Computer computer = new Computer.ComputerBuilder("500 GB ","8 GB")
                .setBluetoothEnabled(true)
                .setGraphicsCardEnabled(false)
                .build();
        printData(computer);

        Computer computer1 = new Computer.ComputerBuilder("400 GB ","16 GB")
                .setBluetoothEnabled(false)
                .setGraphicsCardEnabled(true)
                .build();

        printData(computer1);
    }

    //out
    /*
        computer =
        HDD :500 GB RAM  8 GB
        bluetooth  true
        graphics card false

        computer =
        HDD :400 GB RAM  16 GB
        bluetooth  false
        graphics card true
     */
    static void printData(Computer computer) {
        System.out.println("computer = \n" + "HDD :" +computer.getHDD()
                        + "RAM  "+computer.getRAM() +"\n"
                        + "bluetooth  "+ computer.isBluetoothEnabled() +"\n"
                        + "graphics card " + computer.isGraphicsCardEnabled()
                );
    }
}
