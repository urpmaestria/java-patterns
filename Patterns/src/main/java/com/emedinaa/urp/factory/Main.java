package com.emedinaa.urp.factory;

/**
    Factory Pattern
    https://www.digitalocean.com/community/tutorials/factory-design-pattern-in-java
 */
public class Main {

    public static void main(String[] args) {
        Computer pc = ComputerFactory.getComputer(ComputerType.PC, "4 GB", "500 GB", "2.4 GHz");
        printData(pc);

        Computer server = ComputerFactory.getComputer(ComputerType.SERVER, "8 GB", "1 TB", "2.9 GHz");
        printData(server);
    }

    //out
    /*
        computer =
        RAM :4 GBHDD  500 GB
        CPU  2.4 GHz

        computer =
        RAM :8 GBHDD  1 TB
        CPU  2.9 GHz
     */

    static void printData(Computer computer) {
        System.out.println("computer = \n" + "RAM :" + computer.getRAM()
                + "HDD  " + computer.getHDD() + "\n"
                + "CPU  " + computer.getCPU()
        );
    }
}
