package com.emedinaa.urp.factory;

public class ComputerFactory {

    public static Computer getComputer(String type, String ram, String hdd, String cpu) {
        if ("PC".equalsIgnoreCase(type)) return new PC(ram, hdd, cpu);
        else if ("Server".equalsIgnoreCase(type)) return new Server(ram, hdd, cpu);

        return null;
    }

    public static Computer getComputer(ComputerType type, String ram, String hdd, String cpu) {
        Computer computer;
        switch (type) {
            case PC:
                computer = new PC(ram, hdd, cpu);
                break;
            case SERVER:
                computer = new Server(ram, hdd, cpu);
                break;
            default:
                computer = null;
        }
        return computer;
    }
}
