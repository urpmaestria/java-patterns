package com.emedinaa.urp.dao;

import java.util.List;

public interface StudentDao {

    void populate(List<Student> students);

    List<Student> getAll();

    Student getStudentByCode(int code);

    void add(Student student);

    void update(Student student);

    void delete(Student student);
}
