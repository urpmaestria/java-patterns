package com.emedinaa.urp.dao;

import java.util.ArrayList;
import java.util.List;

/**
 * DAO Pattern
 */
public class Main {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        List<Student> students = new ArrayList();
        Student student1 = new Student("Robert", 0);
        Student student2 = new Student("John", 1);
        Student student3 = new Student("Pepito", 2);
        students.add(student1);
        students.add(student2);
        students.add(student3);

        StudentDao studentDao = new StudentLocalDao();
        studentDao.populate(students);
        printData(studentDao.getAll());

        //add
        Student student = new Student("José", 3);
        studentDao.add(student);
        printData(studentDao.getAll());

        //update
        student.setName("José updated");
        studentDao.update(student);
        printData(studentDao.getAll());

        //delete
        studentDao.delete(student);
        printData(studentDao.getAll());

    }

    //out
    /*
        Student: Code 0 name  Robert
        Student: Code 1 name  John
        Student: Code 2 name  Pepito

        student add 3 José
        Student: Code 0 name  Robert
        Student: Code 1 name  John
        Student: Code 2 name  Pepito
        Student: Code 3 name  José

        student update - code : 3 name : José updated
        Student: Code 0 name  Robert
        Student: Code 1 name  John
        Student: Code 2 name  Pepito
        Student: Code 3 name  José updated

        student delete - code 3 name : José updated
        Student: Code 0 name  Robert
        Student: Code 1 name  John
        Student: Code 2 name  Pepito

     */

    static void printData(List<Student> students) {
        students.forEach(element -> {
            System.out.println("Student: Code " + element.getCode() + " name  " + element.getName());
        });
    }
}
