package com.emedinaa.urp.dao;

import java.util.ArrayList;
import java.util.List;

public class StudentLocalDao implements StudentDao {

    List<Student> students = new ArrayList<>();

    @Override
    public void populate(List<Student> students) {
        this.students.clear();
        this.students.addAll(students);
    }

    @Override
    public List<Student> getAll() {
        return students;
    }

    @Override
    public Student getStudentByCode(int code) {
        Student student = students.get(code);
        return student;
    }

    @Override
    public void add(Student student) {
        if (students.contains(student)) {
            return;
        }
        students.add(student);
        System.out.println("student add  -  code: " + student.getCode() +" name : "+ student.getName());
    }

    @Override
    public void update(Student student) {
        Student uStudent = students.get(student.getCode());
        if (uStudent != null) {
            uStudent.setName(student.getName());
            System.out.println("student update - code : " + student.getCode() +" name : "+ student.getName());
        }
    }

    @Override
    public void delete(Student student) {
        Student dStudent = students.get(student.getCode());
        if (dStudent != null) {
            students.remove(dStudent);
            System.out.println("student delete - code " + student.getCode() +" name : "+ student.getName());
        }
    }
}
