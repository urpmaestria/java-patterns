package com.emedinaa.urp.singleton;

public final class Database {

    private static Database instance;

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    void query(String sql) {
        System.out.println("query = " + sql);
    }
}
