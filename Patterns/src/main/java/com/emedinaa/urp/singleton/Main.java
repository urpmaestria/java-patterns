package com.emedinaa.urp.singleton;

/**
 * Singleton Pattern
 * te permite crear una única instancia de una clase. En este caso la clase Database
 */
public class Main {

    public static void main(String[] args) {
        Database database = Database.getInstance();
        database.query("SELECT * FROM PRODUCTS");

        Database database1 = Database.getInstance();
        database1.query("SELECT * FROM USERS");


        System.out.println("database "+database);
        System.out.println("database1 "+database1);

        //out
        /*
            query = SELECT * FROM PRODUCTS
            query = SELECT * FROM USERS
            database com.emedinaa.urp.singleton.Database@3cd1f1c8
            database1 com.emedinaa.urp.singleton.Database@3cd1f1c8
         */
    }
}
