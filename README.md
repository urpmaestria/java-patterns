# java-patterns



## Environment

Intellij IDEA (Community version) [https://www.jetbrains.com/idea/](https://www.jetbrains.com/idea/)

JDK 11 [https://sdkman.io/jdks](https://sdkman.io/jdks)

##  Samples

Singleton Pattern

Dao Pattern

Builder Pattern

Factory Pattern

Data Transfer Object Pattern (DTO/VO)


## References

https://refactoring.guru

https://www.digitalocean.com/community/tutorials/builder-design-pattern-in-java

https://www.digitalocean.com/community/tutorials/factory-design-pattern-in-java
